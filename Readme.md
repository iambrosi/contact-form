# Contact form application

Displays a contact form, which supports field validation and spam filtering

## Form validation

* Name field is required and cannot be empty
* Email field must be a well formatted email address
* Message field is required and cannot be empty
* CSRF field is required and it has to be a valid hash

### CSRF protection

THe CSRF field is populated with a hash created using a secret key and the timestamp. This hash can be decoded by the application but not by the user. On form submit, the hash is validated against the current timestamp.
