<?php

use Test\Form\ContactForm;
use Test\Template\Template;

try {
    require __DIR__.'/configure.php';

    // This file defines de container and its services
    $container = require __DIR__.'/configure.php';

    // Create contact form
    $form = new ContactForm($container, $container->get('logger'));

    // -- Layout template
    $layout = new Template('layout.php');

    $layout->setVar('container', $container);
    $layout->setVar('form', $form);
    $layout->setVar('codeGuessed', false);
    $layout->setVar('initialPage', 'about');

    // Check received data via POST
    if (isset($_SERVER['REQUEST_METHOD']) && 'POST' == $_SERVER['REQUEST_METHOD']) {
        $form->bind($_POST);

        // Send the email if the received data is correct
        if ($form->isValid()) {
            $container->get('logger')->info('Valid form');

            $message = new Test\Mail\Message();
            $message
                ->setTo($container->getParameter('email_to'))
                ->setSubject(sprintf('New message from %s', $form->getField('name')->getValue()))
                ->setMessage($form->getField('message')->getValue());

            $container->get('mailer')->send($message);
            $layout->setVar('codeGuessed', true);
        } else {
            $container->get('logger')->error('Invalid form');
            $layout->setVar('initialPage', 'contacts');
        }
    }

    // Displays the template
    echo $container->get('renderer')->renderTemplate($layout);

} catch (Exception $e) {
    http_response_code(500);
    echo 'There was an application error. Please try again later.';
}
