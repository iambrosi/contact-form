<?php

namespace Test\Log;

/**
 * Class Logger
 *
 * Simple logging system. For real applications I would
 * use Monolog(https://github.com/seldaek/monolog)
 *
 * Example:
 * <code>
 * $logger = new Test\Log\Logger();
 *
 * $logger->info('My log message');
 * $logger->error('My error message');
 * $logger->warning('My warning message');
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Logger
{
    const TYPE_INFO    = 1;
    const TYPE_WARNING = 2;
    const TYPE_ERROR   = 3;

    /**
     * @var array
     */
    private $handlers;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->handlers = array();
    }

    /**
     * Registers a callback function to manage the log message
     *
     * @param callable $handler
     *
     * @return Logger
     */
    public function registerHandler(\Closure $handler)
    {
        $this->handlers[] = $handler;

        return $this;
    }

    /**
     * Registers an information message
     *
     * @param string $message
     */
    public function info($message)
    {
        $this->log($message, self::TYPE_INFO);
    }

    /**
     * Registers a warning message
     *
     * @param string $message
     */
    public function warning($message)
    {
        $this->log($message, self::TYPE_WARNING);
    }

    /**
     * Registers an error message
     *
     * @param string $message
     */
    public function error($message)
    {
        $this->log($message, self::TYPE_ERROR);
    }

    /**
     * Registers a log message
     *
     * @param string $message
     * @param int    $type
     */
    public function log($message, $type = self::TYPE_INFO)
    {
        foreach ($this->handlers as $handler) {
            call_user_func_array($handler, array($message, $type));
        }
    }
}
