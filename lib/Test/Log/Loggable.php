<?php

namespace Test\Log;

/**
 * Interface Loggable
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
interface Loggable
{
    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger);
}
