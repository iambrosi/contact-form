<?php

namespace Test\Mail;

use Test\Log\Loggable;
use Test\Log\Logger;

/**
 * Class Mail
 *
 * Simple mailer class to manage email sending.
 *
 * For real applications I would use SwiftMailer(http://swiftmailer.org/).
 * It provides support different mail transports among other things
 *
 * Example:
 * <code>
 * $mail = new Test\Mail\Mail();
 * $message = new Test\Mail\Message();
 * ...
 * $mail->send($message);
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Mail implements Loggable
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Sends a message according to a Message instance
     *
     * @param Message $message
     *
     * @return bool
     */
    public function send(Message $message)
    {
        $this->logger->info(sprintf('Sending message with subject "%s" to %s', $message->getSubject(), $message->getTo()));

        return mail($message->getTo(), $message->getSubject(), $message->getMessage());
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }
}
