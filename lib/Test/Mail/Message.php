<?php

namespace Test\Mail;

/**
 * Class Message
 *
 * It manages a mail message
 *
 * Example:
 * <code>
 * $message = new Test\Mail\Message();
 * $message->setTo('me@example.com');
 * $message->setSubject('My message subject');
 * $message->setMessage('My message content');
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Message
{
    /**
     * @var string
     */
    private $to;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $message;

    /**
     * @param string $to
     *
     * @return Message
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $message
     *
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $subject
     *
     * @return Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
}
