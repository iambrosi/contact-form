<?php

namespace Test;

/**
 * Class Container
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Container
{
    /**
     * @var array
     */
    private $services = array();

    /**
     * @var array
     */
    private $parameters = array();

    /**
     * Registers a service
     *
     * @param string $name
     * @param mixed  $service
     */
    public function set($name, $service)
    {
        $this->services[$name] = $service;
    }

    /**
     * Retrieves a previously registered service
     *
     * @param  string                    $name The name of the service
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function get($name)
    {
        if (!isset($this->services[$name])) {
            throw new \InvalidArgumentException('Non-existent service '.$name);
        }

        return $this->services[$name];
    }

    /**
     * Register a parameter
     *
     * @param  string                    $name
     * @param  scalar                    $value
     * @throws \InvalidArgumentException
     */
    public function setParameter($name, $value)
    {
        if (!is_scalar($value)) {
            throw new \InvalidArgumentException('Only scalar values are allowed as parameters');
        }

        $this->parameters[$name] = $value;
    }

    /**
     * Returns a previously registered parameter
     *
     * @param  string $name
     * @return string mixed
     */
    public function getParameter($name)
    {
        return $this->parameters[$name];
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }
}
