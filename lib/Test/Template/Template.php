<?php

namespace Test\Template;

/**
 * Class Template
 *
 * Mini template engine for this app.
 * For real applications I would use Twig(http://twig.sensiolabs.org),
 * which is a really powerful template engine
 *
 * Example:
 * <code>
 * $template = new Test\Template\Template('/path/to/file');
 * $template->setVar('varName', 'value');
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Template
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var array
     */
    private $vars;

    /**
     * @param string $file
     */
    public function __construct($file)
    {
        $this->file = $file;
        $this->vars = array();
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Register a template var
     *
     * @param string $name
     * @param mixed  $value
     */
    public function setVar($name, $value)
    {
        $this->vars[$name] = $value;
    }

    /**
     * Returns the value of a previously registered var
     *
     * @param  string $name
     * @param  mixed  $default
     * @return mixed
     */
    public function getVar($name, $default = null)
    {
        if (isset($this->vars[$name])) {
            return $this->vars[$name];
        }

        return $default;
    }

    /**
     * Returns all the registered vars
     *
     * @return array
     */
    public function getVars()
    {
        return $this->vars;
    }
}
