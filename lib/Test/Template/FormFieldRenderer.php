<?php

namespace Test\Template;

use Test\Form\Field;
use Test\Log\Loggable;
use Test\Log\Logger;

/**
 * Class FormFieldRenderer
 *
 * Handles the template rendering of a form field instance
 *
 * Example:
 * <code>
 * $field = new Test\Form\Field('text', 'example');
 *
 * $formFieldRenderer = new Test\Template\FormFieldRenderer($renderer);
 * $formFieldRenderer->render($field);
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class FormFieldRenderer implements Loggable
{

    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Construct
     *
     * @param Renderer $renderer
     */
    public function __construct(Renderer $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Renders a form field
     *
     * @param \Test\Form\Field $field
     *
     * @return string
     * @throws \Exception
     */
    public function render(Field $field)
    {
        switch ($field->getType()) {
            case 'text':
            case 'email':
                $templateFile = 'form_field_input.php';
                break;
            case 'hidden':
                $templateFile = 'form_field_input_hidden.php';
                break;
            case 'textarea':
                $templateFile = 'form_field_textarea.php';
                break;
            default:
                throw new \Exception('Unsupported field type '.$field->getType());
        }

        $template = new Template($templateFile);
        $template->setVar('type', $field->getType());
        $template->setVar('label', $field->getLabel());
        $template->setVar('attributes', $field->getAttributes());
        $template->setVar('value', $field->getValue());

        $this->logger->info('Rendering form field '.$field->getName());

        return $this->renderer->renderTemplate($template);
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }
}
