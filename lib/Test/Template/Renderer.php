<?php

namespace Test\Template;

use Test\Log\Loggable;
use Test\Log\Logger;

/**
 * Class Renderer
 *
 * Example:
 * <code>
 * $template = new Test\Template\Template('/path/to/file');
 *
 * $renderer = new Test\Template\Renderer('/path/to/templates/dir')
 * echo $renderer->renderTemplate($template);
 *
 * echo $renderer->render('template.php', array('var1' => 'value'));
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Renderer implements Loggable
{

    /**
     * @var string
     */
    private $templatePath;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param string $templatePath
     */
    public function __construct($templatePath)
    {
        $this->templatePath = $templatePath;
    }

    /**
     * Renders a given template
     *
     * @param Template $template
     *
     * @return string
     * @throws \Exception
     */
    public function renderTemplate(Template $template)
    {
        $file = $this->getTemplateFilePath($template->getFile());
        if (!is_readable($file)) {
            throw new \Exception('Template file does not exists or is not readable');
        }

        return $this->render($file, $template->getVars());
    }

    /**
     * Renders a given template file
     *
     * @param string $file
     * @param array  $vars
     *
     * @return string
     * @throws \Exception
     */
    public function render($file, array $vars = array())
    {
        $this->logger->info('Rendering template file '.$file);

        extract($vars);

        ob_start();
        ob_implicit_flush(0);

        try {
            require($file);
        } catch (\Exception $e) {
            ob_end_clean();
            throw $e;
        }

        return ob_get_clean();
    }

    /**
     * Returns the full path of a template file
     *
     * @param $file
     *
     * @return string
     */
    private function getTemplateFilePath($file)
    {
        if ('/' != substr($file, 0, 1)) {
            $file = $this->templatePath.'/'.$file;
        }

        return str_replace('/', DIRECTORY_SEPARATOR, $file);
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }
}
