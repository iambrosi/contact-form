<?php

namespace Test\Form;

use Test\Log\Logger;
use Test\Container;

/**
 * Class ContactForm
 * It manages a Form instance for the Contact application
 *
 * Example:
 * <code>
 * $form = new Test\Form\ContactForm($container, $logger);
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class ContactForm extends Form
{
    /**
     * @var \Test\Container
     */
    private $container;

    /**
     * Constructor
     *
     * @param Container $container
     * @param Logger    $logger
     */
    public function __construct(Container $container, Logger $logger)
    {
        $this->container = $container;
        parent::__construct($logger);
    }

    protected function configure()
    {
        // Form Fields
        $this->registerFields();

        // Anti-spam filter. The created hash is used to compare the data and timestamp received
        $csrfMd5 = md5($this->container->getParameter('csrf_secret').($time = time()).__FILE__);
        $this->getField('csrf')->setValue(base64_encode($csrfMd5.'|'.$time))->lock();

        // Form Validators
        $this->registerValidators();
    }

    private function registerFields()
    {
        // Name field
        $this->registerField(new Field('text', 'name', 'Name', array('placeholder' => 'Your name', 'required' => 'required')));

        // Email field
        $this->registerField(new Field('email', 'email', 'Email', array('placeholder' => 'Your email address', 'required' => 'required')));

        // Message field
        $this->registerField(new Field('textarea', 'message', 'Message', array('placeholder' => 'Your message', 'required' => 'required', 'rows' => 4)));

        // CSRF validation field
        $this->registerField(new Field('hidden', 'csrf', null, array('required' => 'required')));
    }

    private function registerValidators()
    {
        $container = $this->container;

        /** @var $logger \Test\Log\Logger */
        $logger = $container->get('logger');

        // CSRF validator
        $this->registerValidator(
            'csrf', function ($value) use ($container, $logger) {
                $invalidMessage = 'CSRF attact detected. Please, try submitting the form again.';

                $base64Decoded = base64_decode($value);
                if (false === $base64Decoded) {
                    $logger->error('Invalid decoded data for CSRF');

                    return $invalidMessage;
                }

                list($md5, $time) = explode('|', $base64Decoded);
                if (empty($md5) || empty($time) || !is_numeric($time)) {
                    $logger->error('Invalid decoded sections for CSRF');

                    return $invalidMessage;
                }

                // Form expires 10 minutes after created
                $diff = (time() - $time);
                if ($diff < 0 || $diff > (10 * 60)) {
                    $logger->error('Form expired for CSRF:'.$diff);

                    return $invalidMessage;
                }

                if ($md5 != md5($container->getParameter('csrf_secret').$time.__FILE__)) {
                    $logger->error('Invalid md5 for CSRF');

                    return $invalidMessage;
                }

                return true;
            }
        );

        // Name validator
        $this->registerValidator(
            'name', function ($value) {
                return empty($value) ? 'The name cannot be empty' : true;
            }
        );

        // Email validator
        $this->registerValidator(
            'email', function ($value) {
                // The email address should match the regex
                return preg_match('/^[\w\.\-]+\@[a-zA-Z][\w\.\-]+[a-zA-Z]$/', $value) ? true : 'The email does not have a valid format';
            }
        );

        // Message validator
        $this->registerValidator(
            'message', function ($value) {
                return empty($value) ? 'The message cannot be empty' : true;
            }
        );
    }
}
