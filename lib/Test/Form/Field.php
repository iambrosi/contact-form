<?php

namespace Test\Form;

/**
 * Class Field
 *
 * It manages a Form field
 *
 * Example:
 * <code>
 * $field = new Test\Form\Field('text', 'example', 'Example field');
 *
 * $field->setAttribute('class', 'my-class');
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Field
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $value;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var bool
     */
    private $locked;

    /**
     * Constructor
     *
     * @param string $type       The type of field
     * @param string $name       The name of the field
     * @param string $label      The label of the field
     * @param array  $attributes
     */
    public function __construct($type, $name, $label = null, array $attributes = array())
    {
        $this->type       = $type;
        $this->name       = $name;
        $this->label      = is_null($label) ? $this->name : $label;
        $this->value      = null;
        $this->attributes = array_merge(array('name' => $this->name), $attributes);
        $this->errors     = array();
        $this->locked     = false;
    }

    /**
     * Returns the name of the field
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Registers a new attribute in the field
     *
     * @param string $name
     * @param string $value
     *
     * @return Field
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;

        return $this;
    }

    /**
     * Returns the field attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Returns the label of the field
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Returns the type of fields
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of the field
     *
     * @param string $value
     *
     * @return \Test\Form\Field
     */
    public function setValue($value)
    {
        if (!$this->locked) {
            $this->value = $value;
        }

        return $this;
    }

    /**
     * Returns the value of the field
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the field as required or not
     *
     *
     * @param bool $required
     *
     * @return Field
     */
    public function setRequired($required)
    {
        if ($required) {
            $this->setAttribute('required', 'required');
        } else {
            unset($this->attributes['required']);
        }

        return $this;
    }

    /**
     * Locks the field, making the value not to change
     *
     * @return Field
     */
    public function lock()
    {
        $this->locked = true;

        return $this;
    }

    /**
     * It allows to change the value of the field
     *
     * @return Field
     */
    public function unlock()
    {
        $this->locked = false;

        return $this;
    }
}
