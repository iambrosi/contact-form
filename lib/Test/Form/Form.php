<?php

namespace Test\Form;

use Test\Log\Logger;

/**
 * Class Form
 *
 * It manages a form element, allowing to validate the received values
 *
 * Example:
 * <code>
 * $form = new Test\Form\Form($logger);
 *
 * $form->registerField(new Test\Form\Field('text', 'example'));
 * $form->registerValidator('example', function ($value) {
 *      return is_null($value) ? 'Invalid value' : true;
 * });
 *
 * $form->bind($_POST);
 *
 * if ($form->isValid()) {
 *      // do something
 * }
 *
 * </code>
 *
 * @author Ismael Ambrosi<ismaambrosi@gmail.com>
 */
class Form
{
    /**
     * @var array
     */
    private $fields;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var array
     */
    private $validators;

    /**
     * @var bool
     */
    private $bound;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Constructor
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->fields     = array();
        $this->errors     = array();
        $this->validators = array();
        $this->bound      = false;
        $this->logger     = $logger;

        $this->configure();
    }

    /**
     * Configures the form
     */
    protected function configure()
    {
    }

    /**
     * Registers a new form field
     *
     * @param Field $field
     */
    public function registerField(Field $field)
    {
        $this->fields[$field->getName()] = $field;
    }

    /**
     * Retrieves a previously registered field
     *
     * @param $name
     *
     * @return Field
     * @throws \InvalidArgumentException
     */
    public function getField($name)
    {
        if (!isset($this->fields[$name])) {
            throw new \InvalidArgumentException('Undefined field '.$name);
        }

        return $this->fields[$name];
    }

    /**
     * Returns all the registered fields
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Returns an array with error messages
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Return TRUE if the form has one or more errors, FALSE otherwise
     *
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->errors) > 0;
    }

    /**
     * Registers a validator for a given form field
     *
     * @param string   $fieldName
     * @param callable $closure
     */
    public function registerValidator($fieldName, \Closure $closure)
    {
        $this->validators[$fieldName][] = $closure;
    }

    /**
     * Binds the submitted data with the registered form fields
     *
     * @param array $submittedData
     *
     * @throws \Exception
     */
    public function bind(array $submittedData)
    {
        if (count($diff = array_diff(array_keys($submittedData), array_keys($this->fields)))) {
            throw new \Exception('Unexpected fields '.implode(', ', $diff));
        }

        foreach ($submittedData as $fieldName => $fieldValue) {
            $this->getField($fieldName)->setValue($fieldValue);
        }

        foreach ($this->validators as $fieldName => $callbacks) {
            $this->logger->info('Validating field '.$fieldName);
            foreach ($callbacks as $callback) {
                $fieldValue = isset($submittedData[$fieldName]) ? $submittedData[$fieldName] : $this->getField($fieldName)->getValue();
                if (true !== ($message = call_user_func($callback, $fieldValue))) {
                    $this->logger->info('Invalid data on '.$fieldName.': '.$message);
                    $this->errors[] = $message;
                }
            }
        }

        $this->bound = true;
    }

    /**
     * Returns TRUE if the submitted data has no errors, FALSE otherwise
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->bound && !$this->hasErrors();
    }
}
