<div class="control-group">
    <label class="control-label"<?php if(isset($attributes['id'])) printf(' for="%s"', $attributes['id']);?>><?php echo $label; ?></label>

    <div class="controls">
        <input type="<?php echo $type; ?>" <?php echo !empty($value) ? ' value="'.$value.'"' : '';?> <?php foreach ($attributes as $attrName => $attrValue) printf(' %s="%s"', $attrName,$attrValue); ?>/>
    </div>
</div>
