<div class="control-group">
    <label class="control-label"<?php if(isset($attributes['id'])) printf(' for="%s"', $attributes['id']);?>><?php echo $label; ?></label>

    <div class="controls">
        <textarea <?php foreach ($attributes as $attrName => $attrValue) printf(' %s="%s"', $attrName,$attrValue); ?>><?php echo $value; ?></textarea>
    </div>
</div>
