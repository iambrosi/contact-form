<!DOCTYPE html>
<!-- // ensure STRICT XHTML compliance -->

<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Task for Hire</title>
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet"/>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.min.js"></script>

    <style>
        .hidden {
            display: none;
        }
    </style>
</head>

<body>
    <?php if ($codeGuessed) : ?>
    <div class="container">
        <p class="lead">We have received your message. Thank you!</p>
        <a class="btn btn-info" href="test.php">Go back</a>
    </div>
    <?php else: ?>
    <header class="container">
        <!-- Changed using javascript -->
        <h1>Example</h1>

        <div>
            <ul class="nav nav-pills">
                <li><a class="nav-item" href="#about">about</a></li>
                <li><a class="nav-item" href="#services">services</a></li>
                <li><a class="nav-item" href="#customers">customers</a></li>
                <li><a class="nav-item" href="#contacts">contacts</a></li>
            </ul>
        </div>
    </header>

    <div class="container">
        <section id="about" class="hidden">
            Displaying "About" content
        </section>

        <section id="services" class="hidden">
            Displaying "Services" content
        </section>

        <section id="customers" class="hidden">
            Displaying "Customers" content
        </section>

        <section id="contacts" class="hidden">
            <form id="contact_form" class="form-horizontal" action="test.php" method="post">
                <fieldset>
                    <legend>Contact us!</legend>
                    <?php if ($form->hasErrors()) : ?>
                    <div class="alert">
                        <h4>Warning</h4>
                        <ul class="unstyled">
                            <?php foreach ($form->getErrors() as $error) : ?>
                            <li><?php echo $error;?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif;?>
                    <?php
                    foreach ($form->getFields() as $field) {
                        echo $container->get('form_field_renderer')->render($field);
                    }
                    ?>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </section>
    </div>
    <script type="text/javascript">
        /* <![CDATA[ */
        jQuery(document).ready(function () {
            var $ = jQuery, fadeActive = function (id) {
                $(id).fadeIn('fast', function () {
                    $(this).removeClass('hidden');
                });
            }, show = function () {
                var id = jQuery(this).attr('href'), siblings = $(id).siblings(':not(.hidden)');

                if (siblings.length) {
                    siblings.fadeOut('fast', function () {
                        $(this).addClass('hidden');
                        fadeActive(id);
                    });
                } else {
                    fadeActive(id);
                }
                $(this).parent('li').addClass('active').siblings('li').removeClass('active');
                $('header h1').html($(this).html());

                return false;
            };

            jQuery('a.nav-item').click(show);

            // Displays the initial page
            show.call(jQuery('a[href="#<?php echo $initialPage; ?>"]'));
        });
        /* ]]> */
    </script>
    <?php endif;?>
</body>
</html>
