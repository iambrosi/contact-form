<?php

use Test\Log\Logger;
use Test\Log\Loggable;

// Autoload PSR-0 compliant classes when they are required
spl_autoload_register(
    function ($className) {
        $classFile = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, __DIR__.'/lib/'.$className.'.php');

        if (is_readable($classFile)) {
            require $classFile;

            return class_exists($className, false);
        }

        return false;
    }
);

// App configuration
return call_user_func(
    function () {
        $container = new \Test\Container();

        $container->set('logger', new Logger());

        // Log messages to the STDOUT
        $container->get('logger')->registerHandler(
            function ($message, $type) {
                if (('OS400' != php_uname('s'))) {
                    $outputStream = 'php://stdout';
                } else {
                    $outputStream = 'php://output';
                }

                $stream = $type == \Test\Log\Logger::TYPE_ERROR ? 'php://stderr' : $outputStream;

                fwrite(fopen($stream, 'w'), $message.PHP_EOL);
            }
        );

        $container->set('renderer', $renderer = new Test\Template\Renderer(__DIR__.DIRECTORY_SEPARATOR.'templates'));
        $container->set('form_field_renderer', new Test\Template\FormFieldRenderer($renderer, $container->get('logger')));
        $container->set('mailer', new Test\Mail\Mail());

        foreach ($container->getServices() as $service) {
            if ($service instanceof Loggable) {
                $service->setLogger($container->get('logger'));
            }
        }

        $container->setParameter('email_to', 'me@example.com');
        $container->setParameter('csrf_secret', 'My53cR37K3y');

        return $container;
    }
);
